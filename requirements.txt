django==2.1.2
docker==3.5.1
psycopg2==2.7.4
python-socketio==2.0.0
python-social-auth[django]==0.3.6
vk_api==11.2.1
aiohttp==3.4.4
aiohttp-devtools==0.10.4
djangorestframework == 3.8.2
django-google-charts
django-qsstats-magic
python-dateutil