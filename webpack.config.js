const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const env = process.env.NODE_ENV || 'development';
const isDebug = env !== 'production';

// PLUGINS //
let plugins = [
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
        __DEV__: isDebug,
        'process.env.NODE_ENV': JSON.stringify(env)
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
    new HtmlWebpackPlugin({
        filename: 'index.html',
        inject: true,
        template: 'index.html',
        environment: env,
        chunksSortMode: 'none' // избегаем циклических импортов (https://github.com/vuejs/vue-cli/issues/1669)
    })
];

if (env !== 'development') {
    plugins.push(new webpack.NoEmitOnErrorsPlugin());
}

const context = path.resolve(__dirname, 'src');

module.exports = {
    mode: env,
    context: context,
    devtool: env === 'development' ? 'source-map' : false,
    entry: {
        app: './main'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].min.js',
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.vue', '.css', '.json'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js' // Use the full build
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                ["env", {
                                    "useBuiltIns": false,
                                }],
                            ],
                            plugins: [
                                'transform-runtime',
                                'transform-object-rest-spread',
                                'syntax-dynamic-import'
                            ]
                        }
                    },
                ],
                exclude: /node_modules/
            },
            {
                test: /\.vue$/,
                use: [
                    {loader: 'vue-loader'}
                ],
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    plugins,
    optimization: {
        splitChunks: {
            chunks: 'async',
            name: false
        }
    },
    devServer: {
        proxy: [{path: '/', target: 'http://localhost:80', ws: true}],
        stats: {
            modules: false
        },
        open: true,
        disableHostCheck: true

    },
    stats: {
        modules: false
    }
};
