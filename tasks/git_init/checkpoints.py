from docker.models.containers import ExecResult

from tasks.base import BaseCheckpoint


class GitInitCheckout(BaseCheckpoint):
    help = 'Выполнена инициализация репозитория'

    def check(self):
        result: ExecResult = self.exec('git status')
        return result.exit_code == 0
