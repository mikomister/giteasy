from docker.models.containers import ExecResult

from tasks.base import BaseCheckpoint


class CommitCountCheckpoint(BaseCheckpoint):
    def check(self):
        result: ExecResult = self.exec('git log')
        return result.output.decode().find("ЗДЕСЬ БЫЛА ГАМОРА") == -1
