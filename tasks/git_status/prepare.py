from tasks.base import BasePrepareScript

lines = '\n'.join([
    'Ключи доступа',
    '',
    'random.randint(10000, 99999)',
    'random.randint(10000, 99999)',
    'random.randint(10000, 99999)',
    'random.randint(10000, 99999)',
    'random.randint(10000, 99999)',
])

lines2 = '\n'.join([
    'Ключи доступа 2',
    '',
    'random.randint(10000, 99999)',
    'random.randint(10000, 99999)',
    'random.randint(10000, 99999)',
    'random.randint(10000, 99999)',
    'random.randint(10000, 99999)',
])



class Prepare(BasePrepareScript):
    def get_command(self):
        print('git commit prepare')
        return f'sh -c \'echo "{lines}" > /sandbox/keys.txt && ' \
               f'echo "{lines2}" > /sandbox/keys2.txt &&' \
               f'git init && ' \
               f'git add keys.txt && git commit -am keys' \
               f'\''
