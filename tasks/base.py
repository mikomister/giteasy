from abc import abstractmethod


class BasePrepareScript():
    @abstractmethod
    def prepare(self):
        pass


class BaseCheckpoint(BaseException):
    help = 'Контрольная точка задачи'

    def __init__(self, exec_func):
        self.exec = exec_func

    @abstractmethod
    def check(self):
        pass
