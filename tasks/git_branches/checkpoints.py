from docker.models.containers import ExecResult

from tasks.base import BaseCheckpoint


class BranchExistingCheckpoint(BaseCheckpoint):
    def check(self):
        result: ExecResult = self.exec('git branch')
        return 'feat_timer' in result.output.decode()


class TimerCommitedCheckpoint(BaseCheckpoint):
    def check(self):
        result: ExecResult = self.exec('git log --name-status feat_timer')
        return 'timer.py' in result.output.decode()


class MergedBranchesCheckpoint(BaseCheckpoint):
    def check(self):
        result: ExecResult = self.exec('git branch --merged master')
        return result.output.decode().find('feat_timer') != -1


class MergedInMasterInLogCheckpoint(BaseCheckpoint):
    def check(self):
        result: ExecResult = self.exec('git log --name-status master')
        return 'timer.py' in result.output.decode()
