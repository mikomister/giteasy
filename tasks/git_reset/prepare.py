from tasks.base import BasePrepareScript

hello_py = '\n'.join([
    'def init():',
    '   print(\\"Приветствуем на боту нашего корабля!\\")'
])

route_py = '\n'.join([
    'def route():',
    '   return [1, 5, 72, 289, 472, 13, 7, 42, 3]'
])


class Prepare(BasePrepareScript):
    def get_command(self):
        cmd = f'sh -c \'echo "{hello_py}" > hello.py && ' \
               f'echo "{route_py}" > route.py && ' \
               f'git init && ' \
               f'git add . \''
        print(cmd)
        return cmd
