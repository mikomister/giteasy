from docker.models.containers import ExecResult

from tasks.base import BaseCheckpoint


class SeparateCommitsCheckpoint(BaseCheckpoint):
    def get_last_commit_for_file(self, file):
        result: ExecResult = self.exec(f'git log -p {file}')
        commit_info = result.output.decode().split('\n')[0]
        return commit_info.replace('commit', '').strip()

    def check(self):
        return self.get_last_commit_for_file('hello.py') != self.get_last_commit_for_file('route.py')


class CommitsCountCheckpoint(BaseCheckpoint):
    def check(self):
        result: ExecResult = self.exec('sh -c " git log | grep commit | wc -l"')
        try:
            return int(result.output.decode()) == 2
        except ValueError:
            return False
