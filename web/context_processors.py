import subprocess

from django.conf import settings # import the settings file

def get_version(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {'VERSION': subprocess.check_output(["git", "rev-parse", "HEAD"]).decode().strip()
}