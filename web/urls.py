from django.urls import path
from .views import *

urlpatterns = [
    path('', get_main_page),
    path('accounts/profile/', main_page_redirect),
    path('logout/', logout_user, name='logout'),
    path('profile/', get_vk_info, name='profile'),
    path('commands/', commands_graph),
]
