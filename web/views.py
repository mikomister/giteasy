from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render, redirect, render_to_response
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import Count
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import logout
from core.models.task import InputHistory

from vk_api import vk_api


def get_main_page(request):
    if request.user.is_authenticated:
        return render(request, 'giteasy/main.html')
    return render(request, 'giteasy/auth.html', {'DEBUG': settings.DEBUG})


def main_page_redirect(request):
    return redirect('/')


def logout_user(request):
    logout(request)
    return redirect('/')


def get_vk_info(request):
    social = request.user.social_auth.get(provider='vk-oauth2')
    access_token = social.extra_data['access_token']
    vk_session = vk_api.VkApi(token=access_token)
    vkid = social.extra_data['id']
    vk = vk_session.get_api()
    info = vk.users.get(fields='photo_50,city,verified,sex,bdate,career,education')
    user = request.user
    user.vk_info = info
    user.vk_id = vkid
    user.save()
    return redirect('/')

@staff_member_required
def commands_graph(request):
    list1 = []
    output = []
    commandset = InputHistory.objects.all().values_list('command_type').annotate(Count('command_type'))
    for command in commandset:
        str1 = str(command)
        str1 = str1.replace("'", "")
        str1 = str1.replace("(", "")
        str1 = str1.replace(")", "")
        str1 = str1.split(',')
        list1.append(str1[0])
        list1.append(int(str1[1]))
        output.append(list1)
        list1 = []
    return render_to_response('giteasy/graph.html',{'values': output})
