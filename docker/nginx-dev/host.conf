access_log  /var/log/nginx/access.log  main;

server {
    listen 80;

    charset UTF-8;
    set $root_path '/app/dist';

    # static
    location ~* ^/(css|images|js|fonts|videos|documents)/(.+)$ {
        root $root_path;
    }

    location ~* ^/socket.io(.+)$  {
        proxy_pass http://wscontainer:8000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

    location ~* ^/containers/(giteasy_[a-z0-9]+)/attach/ws  {
        add_header X-debug-message $1 always;
        proxy_pass http://docker-proxy:2375;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

    # application
    location ~ / {
        proxy_pass http://python:8000;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Protocol "";
        proxy_set_header X-Forwarded-Ssl "";
        client_max_body_size 100M;
    }

    # application
    location / {
        root $root_path;
        try_files $uri $uri.html;
    }

    access_log  /dev/stdout main;
    error_log   stderr error;

    location ~* ^/(uploads)/.+\.(php|php3|php4|php5|phtml|phps|pl|pm)$ {
        deny all;
    }


    location ~* ^/static/(.+)$ {
        root '/app';
        access_log        off;
        log_not_found     off;
        expires           30d;
    }

    location ~* ^/dist/(.+)$ {
        root '/app';
        access_log        off;
        log_not_found     off;
        expires           30d;
    }
}
