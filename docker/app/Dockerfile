FROM node:8.11-alpine

WORKDIR /app

COPY ./package.json .
COPY ./package-lock.json .

RUN npm i

COPY ./webpack.config.js .
COPY ./src ./src
COPY ./tasks ./tasks

RUN NODE_ENV=production npm run build-prod

FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE git_easy.settings

RUN apk add --no-cache gettext build-base linux-headers pcre-dev uwsgi-python3 gcc postgresql-dev git && pip install uwsgi

WORKDIR /app

COPY ./requirements.txt .

RUN pip install -r requirements.txt

COPY ./docker/nginx /etc/nginx/conf.d

COPY . .

USER $MOD_WSGI_USER:$MOD_WSGI_GROUP

CMD python manage.py migrate && uwsgi --socket :8000 --module git_easy.wsgi --processes=5 \
    --harakiri=20 \
    --max-requests=5000 \
    --enable-threads

COPY --from=0 /app/dist /app/dist

RUN python manage.py collectstatic --no-input

COPY . .
