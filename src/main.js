import Vue from 'vue'
import {sync} from 'vuex-router-sync'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import store from './store'
import 'bootswatch/dist/superhero/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {wsConnect} from "./services/websocket";

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(BootstrapVue);

sync(store, router);

wsConnect();

const app = new Vue({
    router,
    store,
    ...App
});

app.$mount('#app');

// assuming Webpack's HMR API.
// https://webpack.js.org/guides/hot-module-replacement/
if (module.hot) {
    const api = require('vue-hot-reload-api');

    // make the API aware of the Vue that you are using.
    // also checks compatibility.
    api.install(Vue);

    // compatibility can be checked via api.compatible after installation
    if (!api.compatible) {
        throw new Error('vue-hot-reload-api is not compatible with the version of Vue you are using.')
    }

    // indicate this module can be hot-reloaded
    module.hot.accept();

    // if (!module.hot.data) {
    //     // for each component option object to be hot-reloaded,
    //     // you need to create a record for it with a unique id.
    //     // do this once on startup.
    //     api.createRecord('very-unique-id', myComponentOptions)
    // } else {
    //     // if a component has only its template or render function changed,
    //     // you can force a re-render for all its active instances without
    //     // destroying/re-creating them. This keeps all current app state intact.
    //     api.rerender('very-unique-id', myComponentOptions)
    //
    //     // --- OR ---
    //
    //     // if a component has non-template/render options changed,
    //     // it needs to be fully reloaded. This will destroy and re-create all its
    //     // active instances (and their children).
    //     api.reload('very-unique-id', myComponentOptions)
    // }
}

export {app, router, store}
