import {Terminal} from 'xterm';
import * as attach from 'xterm/lib/addons/attach/attach';
import * as fit from 'xterm/dist/addons/fit/fit'
import * as winptyCompat from 'xterm/dist/addons/winptyCompat/winptyCompat'
import 'xterm/dist/xterm.css';

Terminal.applyAddon(attach);
Terminal.applyAddon(fit);
Terminal.applyAddon(winptyCompat);

export default function init(ref) {
    const term = new Terminal();
    term.clear();
    term.open(ref);
    term.focus();
    return term;
}
