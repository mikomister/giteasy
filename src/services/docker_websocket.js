export function dockerConnect(containerId, onopen, onclose) {
    const proto = location.protocol === 'https:' ? 'wss' : 'ws';
    const socket = new WebSocket(`${proto}://${location.hostname}:${location.port}/containers/${containerId}/attach/ws?logs=1&stdin=1&stderr=1&stdout=1&stream=1`);
    socket.onopen = function (event) {
        console.log('docker open');
        onopen(event);
    };
    socket.onclose = event => {
        console.log('docker close', event);
        if (socket.force_close === undefined) {
            setTimeout(() => dockerConnect(containerId, onopen), 1000);
        }
        onclose(event);
    };
    return socket;
}
