import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    namespaced: true,
    modules: {},
    state: {
        user: null,
        need_login: false,
        checkpoints: null,
        tasks: null,
        command_stat: null,
        rating_list: [],
        max_progress: 0,
        progress: 0
    },
    mutations: {
        user(state, data) {
            state.user = data;
            state.need_login = false;
        },
        need_login(state) {
            state.need_login = true;
            state.user = null;
        },
        score(state, data) {
            state.score = data;
        },
        checkpoints(state, data) {
            state.checkpoints = data;
        },
        tasks(state, data) {
            console.log('tasks', data);
            state.tasks = data;
        },
        command_stat(state, data) {
            console.log('command stat', data);
            state.command_stat = data;
        },
        max_progress(state, data) {
            state.max_progress = data;
        },
        progress(state, data) {
            state.progress = data;
        },
        rating_list(state, data) {
            state.rating_list = data;
        }
    },
    actions: {

    }
});
