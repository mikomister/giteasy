from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.core.checks import messages
from django.forms import BaseInlineFormSet

from core.models import User, Task, Progress, InputHistory


class UserInlineFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(UserInlineFormSet, self).__init__(*args, **kwargs)
        # Now we need to make a queryset to each field of each form inline
        self.queryset = Progress.objects.filter(user=self.instance)


class ProgressInlineAdmin(admin.TabularInline):
    model = Progress
    formset = UserInlineFormSet
    exclude = ('previous_task_progress', 'created_time', 'checkpoint_user_passed_list')
    readonly_fields = ('task', 'checkpoint_total_count',
                       'checkpoint_user_passed_count', 'is_done', 'modified_time', 'end_time')
    can_delete = False
    extra = 0


class GitEasyUserAdmin(UserAdmin):
    change_form_template = 'admin/user_form.html'
    list_display = ('id', 'email', 'first_name', 'created_time', 'modified_time')
    list_filter = ()
    search_fields = ('id', 'first_name', 'last_name', 'email', 'vk_id')
    ordering = ('created_time',)
    fieldsets = ()
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'user_permissions'),
        }),
    )
    inlines = [ProgressInlineAdmin]

    def response_change(self, request, obj):
        if "_delete_progress" in request.POST:
            Progress.objects.filter(user_id=obj.id).delete()
            InputHistory.objects.filter(user_id=obj.id).delete()
            self.message_user(request, "Прогресс пользователя удален", level=messages.INFO)
        return super().response_change(request, obj)


class GitEasyTaskAdmin(admin.ModelAdmin):
    def has_change_permission(self, request, obj=None):
        return False


class GitEasyProgressAdmin(admin.ModelAdmin):
    def has_change_permission(self, request, obj=None):
        return False

    list_display = ('get_task', 'user', 'is_done', 'created_time', 'modified_time')

    def get_task(self, obj):
        return obj.task.title

    get_task.short_description = 'title'


class GitEasyInputHistoryAdmin(admin.ModelAdmin):
    def has_change_permission(self, request, obj=None):
        return False

    list_display = ('command_type', 'user_command', 'user')


admin.site.register(User, GitEasyUserAdmin)
admin.site.register(Task, GitEasyTaskAdmin)
admin.site.register(Progress, GitEasyProgressAdmin)
admin.site.register(InputHistory, GitEasyInputHistoryAdmin)
