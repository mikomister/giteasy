# Generated by Django 2.1.2 on 2018-12-15 21:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20181119_0216'),
    ]

    operations = [
        migrations.AddField(
            model_name='inputhistory',
            name='task',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='core.Task'),
        ),
        migrations.AlterField(
            model_name='inputhistory',
            name='command_type',
            field=models.CharField(choices=[('init', 'git init'), ('clone', 'git clone'), ('status', 'git status'), ('diff', 'git diff'), ('add', 'git add'), ('commit', 'git commit'), ('commit --amend', 'git commit --amend'), ('log', 'git log'), ('blame', 'git blame'), ('reset', 'git reset'), ('checkout', 'git checkout'), ('revert', 'git revert'), ('remote', 'git remote'), ('fetch', 'git fetch'), ('pull', 'git pull'), ('push', 'git push'), ('branch', 'git branch'), ('merge', 'git merge'), ('mergetool', 'git mergetool'), ('rm', 'git rm'), ('rebase', 'git rebase')], max_length=10),
        ),
    ]
