# Generated by Django 2.1.2 on 2018-11-19 02:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='vk_id',
            field=models.IntegerField(null=True),
        ),
    ]
