from django.core.management.base import BaseCommand
from core.services.containers import ContainerService


class Command(BaseCommand):
    help = 'Delete user containers'

    def handle(self, *args, **kwargs):
        ContainerService.delete_user_containers()
