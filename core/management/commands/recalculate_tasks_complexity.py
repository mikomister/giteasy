from django.core.management.base import BaseCommand

from core.services.tasks import recalculate_tasks_complexity


class Command(BaseCommand):
    def handle(self, *args, **options):
        recalculate_tasks_complexity()
