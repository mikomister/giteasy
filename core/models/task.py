from django.contrib.postgres.fields import JSONField
from django.db import models

from core.enums import COMMANDS
from core.models.user import User
from core.models.base import BaseModel


class Task(models.Model):
    """
    Задание
    """
    title = models.CharField(max_length=200)
    slug = models.SlugField()

    # сложность (суммируется с сложностей гит команд)
    complexity = models.IntegerField(blank=True, null=True, default=0)

    # список команд, которые в ней используются (JSONB array)
    commands = JSONField(default=list, blank=True)

    def __str__(self):
        return f'{self.title} ({self.commands})'


class Progress(BaseModel):
    """
    Выполнение задачи (прогресс)
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    previous_task_progress = models.ForeignKey('Progress', on_delete=models.SET_NULL, null=True)

    # колво контрольных точек в задании всего
    checkpoint_total_count = models.IntegerField(blank=True, null=True, default=0)

    # колво пройденных человеком контрольных точек
    checkpoint_user_passed_count = models.IntegerField(blank=True, null=True, default=0)

    # список пройденных контрольных точек
    checkpoint_user_passed_list = JSONField(blank=True, default=list)

    # флаг выполнения задания (ставится в true, если колво пройденных контрольных точек = общее колво контрольных точек)
    is_done = models.BooleanField(default=False)

    end_time = models.DateTimeField(null=True, blank=True)  # дата завершения выполнения


class InputHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_command = models.TextField()
    task = models.ForeignKey(Task, on_delete=models.DO_NOTHING, blank=True, null=True)
    command_type = models.CharField(max_length=10, choices=COMMANDS)
