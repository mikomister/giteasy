from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.postgres.fields import JSONField

from core.models.base import BaseModel



class UserManager(BaseUserManager):
    def create_superuser(self, email, password=None):
        root = self.model(email=self.normalize_email(email))
        root.is_superuser = True
        root.set_password(password)
        root.save(using=self._db)
        return root

    def create_user(self, email, password=None):
        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user


class User(PermissionsMixin, AbstractBaseUser, BaseModel):
    objects = UserManager()

    email = models.EmailField(max_length=255, unique=True, verbose_name=_('Email'), blank=True)
    first_name = models.CharField(max_length=255, verbose_name=_('First name'), blank=True, null=True)
    last_name = models.CharField(max_length=255, verbose_name=_('Last name'), blank=True, null=True)
    vk_id = models.IntegerField(null=True, blank=True)
    ws_id = models.CharField(max_length=50)  # последний ID вебсокета, к которому шло подключение
    vk_info = JSONField(blank=True, null=True)
    USERNAME_FIELD = 'email'

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )

    def __str__(self):
        return f'{self.id} {self.first_name}'
