from core.data.commands import COMMANDS_COMPLEXITY

COMMANDS = [(cmd, f'git {cmd}') for cmd in COMMANDS_COMPLEXITY]
