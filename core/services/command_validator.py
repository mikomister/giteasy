from core.enums import COMMANDS
from core.models import User
from core.models.task import InputHistory


def validate_command(command_for_find: str):
    for slug, command in COMMANDS:
        # print('command validator', command_for_find, command, command_for_find in command)
        if command in command_for_find:
            return slug, command
    return None


def validate_and_save_command(user_command: str, user: User):
    find_result = validate_command(user_command)
    if not find_result:
        print('unsupported command', user_command)
        return False

    slug, command = find_result
    InputHistory.objects.create(user=user, user_command=user_command, command_type=slug)
    print(user_command)
    return True
