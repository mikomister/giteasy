import asyncio
import importlib
import inspect

from core.data.commands import COMMANDS_COMPLEXITY
from core.models import User, Progress, Task
from core.services.command_validator import validate_command
from core.services.containers import ContainerService
from tasks.base import BaseCheckpoint


class TaskProgressManagerService():
    def __init__(self, user: User):
        self.user = user
        self.container_service: ContainerService = None

    def get_active_task_progress(self):
        try:
            self.progress = Progress.objects.filter(user=self.user, is_done=False).first()
            self.container_service = ContainerService(self.progress)
            return self.progress
        except Progress.DoesNotExist:
            return None

    def remove_unfinished_tasks(self):
        Progress.objects.filter(user=self.user, is_done=False).delete()

    async def start_task(self, task_slug):
        task = Task.objects.get(slug=task_slug)
        if self.get_active_task_progress():
            self.remove_unfinished_tasks()

        self.progress = Progress.objects.create(task=task, user=self.user)
        self.container_service = ContainerService(self.progress)
        self.container_service.run()
        self.prepare()
        await asyncio.sleep(1)
        return self.progress

    def get_container_name(self):
        return self.container_service.container_name()

    def exec(self, cmd):
        return self.container_service.exec(cmd)

    def connect(self):
        return self.container_service.connect()

    def prepare(self):
        module = importlib.import_module(f'tasks.{self.progress.task.slug}.prepare')
        cmd = module.Prepare().get_command()
        if cmd:
            self.exec(cmd)

    def run_checkpoints(self):
        checkpoints = importlib.import_module(f'tasks.{self.progress.task.slug}.checkpoints')
        classes = inspect.getmembers(checkpoints, inspect.isclass)

        total = 0
        passed = 0

        for classname, checkpoint_cls in classes:
            if classname.find('Base') != -1 or not issubclass(checkpoint_cls, BaseCheckpoint):
                continue

            total += 1
            checkpoint = checkpoint_cls(lambda x: self.container_service.exec_with_code(x))
            is_done = checkpoint.check()

            if is_done:
                passed += 1

            print('- checkpoint', checkpoint.__class__.__name__, is_done)

        self.progress.checkpoint_total_count = total
        self.progress.checkpoint_user_passed_count = passed
        self.progress.is_done = total == passed
        self.progress.save()

        return passed, total, self.progress.is_done


def recalculate_tasks_complexity():
    for task in Task.objects.all():
        complexity = 0
        for command in task.commands:
            if validate_command(command) is None:
                print('WARNING: command', command, 'for task', task, 'not found')
                continue
            complexity += COMMANDS_COMPLEXITY[command.replace('git ', '')]
        task.complexity = complexity
        task.save()
