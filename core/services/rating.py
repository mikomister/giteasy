from core.models import Task, User


def get_max_rating() -> int:
    """Получить максимальный рейтинг -  сумма сложностей задач в системе"""
    return sum(Task.objects.values_list('complexity', flat=True))


def get_user_rating(user: User) -> int:
    """Получить рейтинг пользователя - сумма сложностей задач, которые он прошел"""
    success_tasks_complexity = Task.objects.filter(progress__user=user, progress__is_done=True) \
        .distinct() \
        .values_list('complexity', flat=True)
    return sum(success_tasks_complexity)


def get_rating_list():
    """Получить список пользователей с рейтингом для вывода на сайте. Сортировка по рейтингу"""
    users = User.objects.all()
    users_with_rating = []
    for user in users:
        data = {
            "full_name": "{} {}".format(user.first_name, user.last_name),
            "link": "https://vk.com/{}".format(user.vk_id),
            "photo_url": user.vk_info[0].get('photo_50'),
            "rating": get_user_rating(user)
        }
        users_with_rating.append(data)
    users_with_rating = sorted(users_with_rating, key=lambda x: x['rating'])
    return users_with_rating
