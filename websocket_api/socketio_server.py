from socketio.asyncio_server import AsyncServer
from core.models import User
from core.services.progress import get_user_progress, get_max_progress, get_command_stat
from core.services.rating import get_rating_list
from core.services.recomendations import get_user_tasks_list
from core.services.tasks import TaskProgressManagerService
from core.services.command_validator import validate_and_save_command
from websocket_api.auth import get_user

sio = AsyncServer(async_mode='aiohttp', logger=True, async_handlers=True)

WS_NAMESPACE = '/ws'
DOCKER_WS_NAMESPACE = '/docker'


async def send_user_progress(user, sid):
    await sio.emit('progress', get_user_progress(user), room=sid, namespace=WS_NAMESPACE)


async def send_checkpoints(manager: TaskProgressManagerService, sid: str) -> bool:
    passed, total, is_done = manager.run_checkpoints()
    await sio.emit('checkpoints', {'total': total, 'passed': passed, 'is_done': is_done}, room=sid,
                   namespace=WS_NAMESPACE)
    return is_done


@sio.on('connect', namespace=WS_NAMESPACE)
async def connect(sid, environ):
    print("connect ", sid)
    user = get_user(environ)
    if user is None:
        await sio.emit('need_login', room=sid, namespace=WS_NAMESPACE)
        await sio.disconnect(sid)
        return

    user.ws_id = sid
    user.save()

    user_data = {
        'id': user.id,
        'full_name': user.first_name + ' ' + user.last_name,
    }

    await sio.emit('user', user_data, room=sid, namespace=WS_NAMESPACE)
    await send_user_progress(user, sid)
    await sio.emit('maxProgress', get_max_progress(), room=sid, namespace=WS_NAMESPACE)


@sio.on('startTask', namespace=WS_NAMESPACE)
async def start_task(sid, task_slug):
    user = User.objects.get(ws_id=sid)  # TODO decorator
    print(sid, task_slug, user)
    manager = TaskProgressManagerService(user)
    await manager.start_task(task_slug)
    await sio.emit('startOk', manager.get_container_name(), room=sid, namespace=WS_NAMESPACE)
    await send_checkpoints(manager, sid)


@sio.on('tasks', namespace=WS_NAMESPACE)
async def end_task(sid):
    user = User.objects.get(ws_id=sid)  # TODO decorator
    await sio.emit('tasks', get_user_tasks_list(user), room=sid, namespace=WS_NAMESPACE)


@sio.on('score', namespace=WS_NAMESPACE)
async def get_score(sid):
    user = User.objects.get(ws_id=sid)  # TODO decorator
    print(sid, user)
    await send_user_progress(user, sid)


@sio.on('get_command_stat', namespace=WS_NAMESPACE)
async def get_user_command_stat(sid):
    user = User.objects.get(ws_id=sid)  # TODO decorator
    print(sid, user)
    stat = get_command_stat(user)
    await sio.emit('command_stat', stat, room=sid, namespace=WS_NAMESPACE)


@sio.on('get_rating', namespace=WS_NAMESPACE)
async def get_users_rating(sid):
    rating = get_rating_list()
    await sio.emit('rating', rating, room=sid, namespace=WS_NAMESPACE)


@sio.on('cmd', namespace=WS_NAMESPACE)
async def command(sid, cmd):
    user = User.objects.get(ws_id=sid)  # TODO decorator
    print(sid, cmd, user)
    cmd = cmd.strip()

    print(cmd)
    validate_and_save_command(cmd, user=user)
    manager = TaskProgressManagerService(user)
    manager.get_active_task_progress()
    is_done = await send_checkpoints(manager, sid)
    if is_done:
        await sio.emit('done', '', room=sid, namespace=WS_NAMESPACE)
        await send_user_progress(user, sid)
